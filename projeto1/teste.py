import csv
import matplotlib.pyplot as plt

# Vamos ler os dados como uma lista
print("Lendo o documento...")
with open("chicago.csv", "r") as file_read:
    reader = csv.reader(file_read)
    data_list = list(reader)

def column_to_list(data, index):
    column_list = []

    for column in data:
        column_list.append(column[index])
    # Dica: Você pode usar um for para iterar sobre as amostras, pegar a feature pelo seu índice, e dar append para uma lista
    return column_list

# Vamos mudar o data_list para remover o cabeçalho dele.
data_list = data_list[1:]


trip_duration_list = column_to_list(data_list, 2)
min_trip = float(trip_duration_list[0])
max_trip = 0.
mean_trip = 0.
median_trip = 0.

trip_duration_list_order = []

for trip_duration in trip_duration_list:
    f_trip_duration = float(trip_duration)
    trip_duration_list_order.append(f_trip_duration)

    if min_trip > f_trip_duration:
        min_trip = f_trip_duration
    if max_trip < f_trip_duration:
        max_trip = f_trip_duration
    
    mean_trip += f_trip_duration

mean_trip = mean_trip / len(trip_duration_list)

trip_duration_list_order.sort()

element = len(trip_duration_list_order)
middle = (element-1)//2

if element%2==0:
	median_trip=(trip_duration_list_order[middle+1] + trip_duration_list_order[middle+2]) / 2
else:
	median_trip=trip_duration_list_order[middle+1]*1

print("\nTAREFA 9: Imprimindo o mínimo, máximo, média, e mediana")
print("Min: ", min_trip, "Max: ", max_trip, "Média: ", mean_trip, "Mediana: ", median_trip)

# ------------ NÃO MUDE NENHUM CÓDIGO AQUI ------------
assert round(min_trip) == 60, "TAREFA 9: min_trip com resultado errado!"
assert round(max_trip) == 86338, "TAREFA 9: max_trip com resultado errado!"
assert round(mean_trip) == 940, "TAREFA 9: mean_trip com resultado errado!"
assert round(median_trip) == 670, "TAREFA 9: median_trip com resultado errado!"
# -----------------------------------------------------